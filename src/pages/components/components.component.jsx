import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap';
import GeneralSettings from '../../components/general-settings/general-settings.component';
import ModalPopUp from '../../components/Modal/modal.component';
import Layout from '../../HOC/layout/layout.component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faPlus, faCubes } from '@fortawesome/free-solid-svg-icons';
import './components.styles.scss';



class ComponentsPage extends Component {

    state = {
        components: [],
        isModalShown: false,
        itemToDelete: null
    }

    openModalHandler = () => {
        this.setState({
            isModalShown: true
        })
    }

    removeComponentsHandler = () => {
        const { components, itemToDelete } = this.state;
        if (itemToDelete) {
            const newFilteredComponents = components.filter(componentItem => componentItem.id !== itemToDelete);
            this.setState({
                components: newFilteredComponents,
                isModalShown: false
            })
        }
    }

    closeModalHandler = () => {
        this.setState({
            isModalShown: false
        })
    }

    addComponentHandler = () => {
        const { components } = this.state;
        let id = null ;
        if( components.length !== 0)
        {
            id = components[components.length - 1].id + 1;
            console.log(id);
        }
        else
        {
            id = 1;
        }
        this.setState((prevState) => ({
            components: [...prevState.components, { id: id, label: `Component ${id}` }]
        }));
    }

    showModalAndSetItemToDelete = (componentId) => {
        this.setState({
            isModalShown: true,
            itemToDelete: componentId
        })
    }

    render() {
        const { components, isModalShown } = this.state;
        return (
            <Layout>
                <Row className="mt-3">
                    <Col xs={8}>
                        <h4><strong>Component Configuration</strong></h4>
                        <p>Curriculums / <span style={{fontWeight:'600'}}> Component Configuration</span></p>
                    </Col>
                    <Col xs={4} className="text-right">
                        <button className="btn btn-primary add-component" onClick={this.addComponentHandler}>  <FontAwesomeIcon icon={faPlus}/>  Add Component</button>
                    </Col>
                    <GeneralSettings />
                    <Col className="components">
                        <div className="components__content">
                            <strong>Components</strong>
                            <Row className="mt-3">
                                
                                {
                                    components.length === 0 ? 
                                    <Col className="nocomponents">
                                        <FontAwesomeIcon icon={faCubes}/>
                                        <h3>No Components yet</h3>
                                        <p>Create and configure components for which teachers will be entering marks.</p>
                                    </Col>
                                    :
                                    components &&
                                    components.map((componentItem) => {
                                        return (
                                            <Col xs={4} key={componentItem.id} >
                                                <div className="form-group">
                                                    <Row>
                                                        <Col xs={8}>
                                                            <label htmlFor="">{componentItem.label}</label>
                                                        </Col>
                                                        <Col xs={4} className="text-right">
                                                            <button className="btn removebtn" onClick={() => this.showModalAndSetItemToDelete(componentItem.id)}><FontAwesomeIcon icon={faTrashAlt}/></button>
                                                        </Col>
                                                    </Row>
                                                    <input type="text" className="form-control" placeholder={componentItem.label} />
                                                </div>
                                            </Col>
                                        )
                                    })
                                }
                            </Row>
                        </div>
                        <ModalPopUp isModalShown={isModalShown} closeModalHandler={this.closeModalHandler} removeComponentsHandler={this.removeComponentsHandler} />
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default ComponentsPage;
