import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import './modal.styles.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';



const ModalPopUp = ({ isModalShown, closeModalHandler, removeComponentsHandler }) => {
    return (
        <Modal show={isModalShown} onHide={closeModalHandler} animation={true} className="modal">
           
            <Modal.Body className="modal__body">
                <div className="wrapper">
                    <FontAwesomeIcon icon={faTimesCircle} className="deleteIcon"/> 
                    <h5>Are you sure <br/> you want to delete the component ?</h5>
                </div>
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-center align-items-center">
                <Button className="cancel" variant="secondary" onClick={closeModalHandler}>
                    No, Cancel It
                </Button>
                <Button className="remove" variant="danger" onClick={removeComponentsHandler}>
                    DeleteComponent
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModalPopUp
