import React from 'react'
import { Col, Row } from 'react-bootstrap';
import './general-settings.styles.scss';


const GeneralSettings = () => {
    return (
        <Col xs={12} className="general-settings">
            
            <div className="general-settings__content">
            <strong>General Settings</strong>
                <Row className="select-items mt-2">
                    <Col>
                        <div className="form-group">
                            <label htmlFor="year">Year</label>
                            <select name="year" id="" className="form-control">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </Col>
                    <Col>
                        <div className="form-group">
                            <label htmlFor="year">Grade</label>
                            <select name="year" id="" className="form-control">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </Col>
                    <Col>
                        <div className="form-group">
                            <label htmlFor="year">Name</label>
                            <select name="year" id="" className="form-control">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </Col>
                    <Col>
                        <div className="form-group">
                            <label htmlFor="year">Subject</label>
                            <select name="year" id="" className="form-control">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </Col>
                    <Col>
                        <div className="form-group">
                            <label htmlFor="year">Term</label>
                            <select name="year" id="" className="form-control">
                                <option value="">Select Year</option>
                            </select>
                        </div>
                    </Col>
                </Row>
            </div>
        </Col>
    )
}

export default GeneralSettings
