import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';
import Sidebar from '../../components/sidebar/sidebar.component';
import NavBar from '../../components/navbar/navbar.component';



const Layout = ({ children }) => {
    return (
        <>
            <NavBar />
            <Container fluid>
                <Row style={{ minHeight: '100vh', height: '100vh' }}>
                    <Sidebar />
                    <Col xs={12} sm={10} className="h-100" style={{ background: '#F8F9FA' }}>
                        {children}
                    </Col>
                </Row>
            </Container>
        </>

    )
}

export default Layout
