import React from 'react';
import './App.css';
import ComponentsPage from './pages/components/components.component';


function App() {
  return (
    <>
     <ComponentsPage/>
    </>
  );
}

export default App;
